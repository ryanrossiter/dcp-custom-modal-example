import './style/custom-modal.scss';

/**
 * Proxy for console log/error/etc that will log normally but also
 * append the message to the document body and format objects nicely.
 * 
 * @param {function} origLog - The original logging function that's being proxied
 * @param {string} color - A CSS color string that will be used to style the text
 * @param {...*} args - Any additional arguments will be included in the log message
 */
const newLog = (origLog, color, ...args) => {
  origLog(...args);

  let p = document.createElement('pre');
  p.innerText = args.map(a => typeof a === 'string'? a : JSON.stringify(a, null, 2)).join(' ');
  p.style.color = color;
  document.body.appendChild(p);
}

// Rebind the window's console log and error so that they also
// print to the document body with *colors*
let _consoleLog = console.log,
  _consoleErr = console.error;
console.log = newLog.bind(null, _consoleLog, 'black');
console.error = newLog.bind(null, _consoleErr, 'red');

const main = () => {
  const ClientModal = dcp['client-modal'];

  document.getElementById('alert-button').addEventListener('click', () => {
    ClientModal.alert('Warning! Warning! This is an alert!', {
      submit: "Okay then!",
      title: "This is the alert title",
      onClose: () => console.log('Alert modal onClose was called'),
    })
    .then(() => console.log('Alert modal promise resolved'));
  });

  document.getElementById('confirm-button').addEventListener('click', () => {
    ClientModal.confirm('Did you mean to show this confirm modal?', {
      positive: 'Yes',
      negative: 'No',
      title: 'This is the confirm title',
      onClose: () => console.log('Confirm modal onClose was called'),
    })
    .then((v) => console.log('Confirm modal promise resolved with', v));
  });

  document.getElementById('get-password-button').addEventListener('click', () => {
    ClientModal.getPassword({
      label: 'my keystore'
    }, true, () => console.log('Get password modal onClose was called'))
    .then((v) => console.log('Get password modal promise resolved with', v));
  });

  document.getElementById('get-keystore-file-button').addEventListener('click', () => {
    ClientModal.getKeystoreFile({
      name: 'Keystore Name',
      jobName: 'Job Name',
    }, () => console.log('Get keystore file modal onClose was called'))
    .then((v) => console.log('Get keystore file modal promise resolved with', v));
  });
}

document.addEventListener('DOMContentLoaded', main, false);
